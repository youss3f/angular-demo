import { Component, OnInit } from '@angular/core';
import {StateService} from "../services/state.service";

@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.scss']
})
export class OneComponent implements OnInit {

  checkBoxStateOne: boolean = false;

  constructor(private stateService: StateService) { }

  ngOnInit(): void {
    this.stateService.reusableComponentState.subscribe((state: boolean): void => {
      this.checkBoxStateOne = state;
    })
  }
}
