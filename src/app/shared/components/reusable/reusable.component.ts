import { Component, OnInit } from '@angular/core';
import {StateService} from "../../../services/state.service";

@Component({
  selector: 'app-reusable',
  templateUrl: './reusable.component.html',
  styleUrls: ['./reusable.component.scss']
})
export class ReusableComponent implements OnInit {

  checkBoxState: boolean = false;

  constructor(private stateService: StateService) { }

  ngOnInit(): void {
  }

  // problem happens when we load another component the uncheck behavior doesn't work properly
  checkBoxChanged() {
    this.checkBoxState = !this.checkBoxState;
    this.stateService.reusableComponentState.next(this.checkBoxState);
  }
}
