import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class StateService {

  // Subject behavior, il prend une valeur initial
  reusableComponentState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor() { }
}
