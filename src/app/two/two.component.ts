import { Component, OnInit } from '@angular/core';
import {StateService} from "../services/state.service";

@Component({
  selector: 'app-two',
  templateUrl: './two.component.html',
  styleUrls: ['./two.component.scss']
})
export class TwoComponent implements OnInit {

  checkBoxStateTwo: boolean = false;

  constructor(private stateService: StateService) {
  }

  ngOnInit(): void {
    this.stateService.reusableComponentState.subscribe((state: boolean): void => {
      this.checkBoxStateTwo = state;
    });
  }

}
